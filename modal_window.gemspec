$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "modal_window/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "modal_window"
  spec.version     = ModalWindow::VERSION
  spec.authors     = ["yongoh"]
  spec.email       = ["a.yongoh@gmail.com"]
  spec.homepage    = ""
  spec.summary     = ""
  spec.description = ""
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "event_button"

  spec.add_development_dependency "rails"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency "rspec-html-matchers"
  spec.add_development_dependency "capybara", "3.15"
  spec.add_development_dependency "poltergeist"
  spec.add_development_dependency "puma"
  spec.add_development_dependency "coffee-rails"
  spec.add_development_dependency "sass-rails"
end
