require 'rails_helper'

describe ModalWindow::ApplicationHelper do
  describe "#modal_window" do
    subject do
      h.modal_window("foobar") do
        concat content_tag("div", "(´･ω･｀)", id: "shobon")
        concat content_tag("span", "（＾ω＾）", class: "boon")
      end
    end

    it "は、トリガーとなるチェックボックスを返すこと" do
      is_expected.to have_tag("input.modal_window-trigger#foobar-trigger", with: {type: "checkbox"})
    end

    it "は、モーダルウインドウ要素とラベル要素を含むモーダル背景要素を返すこと" do
      is_expected.to have_tag("div.modal_window-overlay#foobar-overlay"){
        with_tag("label", with: {for: "foobar-trigger"})
        with_tag("div.modal_window-window#foobar-window"){
          with_tag("div#shobon", text: "(´･ω･｀)")
          with_tag("span.boon", text: "（＾ω＾）")
        }
      }
    end
  end
end
