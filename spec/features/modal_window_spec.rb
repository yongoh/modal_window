require 'rails_helper'

feature "modal_window", js: true do
  before do
    visit root_path skip_animation: true
  end

  shared_examples_for "close window" do
    scenario "モーダルウィンドウとオーバーレイ背景を隠す" do
      expect(page).to have_no_selector("div.shobon")
      expect(page).to have_no_selector("div.modal_window-overlay#foobar-overlay")
    end
  end

  context "初期状態" do
    it_behaves_like "close window"
  end

  context "「Open」ボタンをクリックした場合" do
    before do
      click_button "Open"
    end

    scenario "モーダルウィンドウとオーバーレイ背景を表示する" do
      expect(page).to have_selector("div.shobon")
      expect(page).to have_selector("div.modal_window-overlay#foobar-overlay")
    end

    context "「Close」ボタンをクリックした場合" do
      before do
        click_button "Close"
      end

      it_behaves_like "close window"
    end

    context "オーバーレイ背景をクリックした場合" do
      before do
        find("#foobar-overlay").click
      end

      it_behaves_like "close window"
    end
  end
end
