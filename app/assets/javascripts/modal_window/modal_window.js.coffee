window.ModalWindow ?= {}

ModalWindow.SELECTORS = {}
["trigger", "overlay", "window"].forEach (key)->

  # @return [String] #{key}のセレクタ
  ModalWindow.SELECTORS[key.toUpperCase()] = ".modal_window-#{key}"

  # @param [String] id #{key}以外のID属性値
  # @return [String] 渡したIDの要素に対応する#{key}のID属性値
  ModalWindow.SELECTORS["to#{key.camelize()}Id"] = (id)->
    [id.replace(/-(trigger|overlay|window)$/, ""), key].join("-")

# 「開く」ボタン
document.addEventButtonListener "click", "modal_window:open", (button)->
  button.elementOfAction().checked = true

# 「閉じる」ボタン
document.addEventButtonListener "click", "modal_window:close", (button)->
  button.elementOfAction().checked = false

# 開閉トグルボタン
document.addEventButtonListener "click", "modal_window:toggle", (button)->
  trigger = button.elementOfAction()
  trigger.checked = !trigger.checked
