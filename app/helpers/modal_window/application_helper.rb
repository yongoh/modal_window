module ModalWindow
  module ApplicationHelper

    # @param [String] name モーダルウインドウ名
    # @param [Hash] html_attributes ウインドウ要素のHTML属性
    # @return [ActiveSupport::SafeBuffer] モーダルウインドウ
    def modal_window(name, html_attributes = {}, &block)
      html_attributes = {class: "modal_window-window", id: "#{name}-window"}.html_attribute_merge(html_attributes)
      trigger_id = "#{name}-trigger"

      capture do
        concat(tag(:input, type: "checkbox", class: "modal_window-trigger", id: trigger_id))
        concat(content_tag(:div, class: "modal_window-overlay", id: "#{name}-overlay"){
          concat(content_tag(:label, nil, for: trigger_id))
          concat(content_tag(:div, html_attributes){ instance_exec(&block) if block_given? })
        })
      end
    end
  end
end
